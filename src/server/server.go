package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
)

const (
	DEFAULT_PORT = ":8000"
)

type Server struct {
	Port string
}

func NewServer() *Server {
	return &Server{
		Port: DEFAULT_PORT,
	}
}

func (s *Server) ListenAndServe() {
	r := gin.Default()
	if store, err := redis.NewStore(10, "tcp", "localhost:6379", "", []byte("secret")); err != nil {
		log.Fatalf("new redis err:%v", err)
	} else {
		r.Use(sessions.Sessions("session", store))
	}

	r.GET("/info", func(c *gin.Context) {
		session := sessions.Default(c)
		c.String(http.StatusOK, fmt.Sprintf("session:%v", session.Get("kenny")))
	})
	r.POST("/user/login", Login)

	r.Run(s.Port)
}
