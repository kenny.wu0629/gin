package server

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	uuid "github.com/satori/go.uuid"
)

type LoginReq struct {
	User     string `json:"user" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func Login(c *gin.Context) {
	login := LoginReq{}
	if err := c.ShouldBindWith(&login, binding.JSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	uid := login.User
	user := NewUserCtlr(uid)

	if !user.ValidatePassword(login.Password) {
		c.JSON(http.StatusUnauthorized, gin.H{"status": "unauthorized"})
	}

	token := uuid.NewV4().String()

	cookie := &http.Cookie{
		Name:     "session_id",
		Value:    token,
		Path:     "/",
		HttpOnly: true,
	}
	http.SetCookie(c.Writer, cookie)

	saveSession(c, user.uid, token)

	c.JSON(http.StatusOK, gin.H{"status": "you are logged in"})
}

func saveSession(c *gin.Context, id, token string) {
	session := sessions.Default(c)
	session.Set(id, token)
	session.Save()
}
