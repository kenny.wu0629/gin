package main

import (
	api "server"
)

func main() {
	webServer := api.NewServer()
	webServer.ListenAndServe()
}
