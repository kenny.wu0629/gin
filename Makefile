SHELL=/bin/sh

export GOPATH=$(shell pwd)/
export GOBIN=$(shell pwd)/bin
export PATH := $(GOBIN):$(PATH)
export GO15VENDOREXPERIMENT=1

SERVERMAIN := $(shell pwd)/src/build
TARGET_NAME = server
TARGET_BIN = $(foreach n,$(TARGET_NAME),$(n))

all: lib $(TARGET_BIN)

lib:
	test -f $(GOPATH)/bin/glide || (GOPATH=$(GOPATH) go get github.com/Masterminds/glide && rm -rf $(GOPATH)/src/github.com/Masterminds)
	test -f $(GOPATH)/bin/glide || (echo "uninstalled tool glide!" && exit 1);
	make -C $(shell pwd)/src

empty:

$(TARGET_BIN): empty
	@echo build $@
	GOPATH=$(GOPATH):$(shell pwd) go build -o  $@ $(SERVERMAIN)/main.go
